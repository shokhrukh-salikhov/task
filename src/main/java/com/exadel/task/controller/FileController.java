package com.exadel.task.controller;

import com.exadel.task.model.SortType;
import com.exadel.task.service.FileService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/upload")
    public ModelAndView handleFileUpload(
            @RequestParam("file") MultipartFile file,
            @RequestParam("sort") SortType sortType
    ) throws IOException {
        fileService.uploadFile(file, sortType);
        return new ModelAndView("redirect:/result");
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<Resource> download(@PathVariable String fileName) {
        return fileService.loadFileAsResource(fileName);
    }
}
