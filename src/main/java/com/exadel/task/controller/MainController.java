package com.exadel.task.controller;

import com.exadel.task.service.FileService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/result")
    public String result(Model model) {
        model.addAttribute("result", FileService.getResult());
        return "result";
    }
}
