package com.exadel.task.model;

import java.util.List;

public class Result {
    private List<Student> students;
    private String time;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
