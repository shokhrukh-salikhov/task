package com.exadel.task.model;

public enum SortType {
    BUBBLE,
    HEAP,
    MERGE
}
