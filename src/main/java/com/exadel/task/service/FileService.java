package com.exadel.task.service;

import com.exadel.task.model.Result;
import com.exadel.task.model.SortType;
import com.exadel.task.model.Student;
import com.exadel.task.service.sort.BubbleSort;
import com.exadel.task.service.sort.CustomSort;
import com.exadel.task.service.sort.HeapSort;
import com.exadel.task.service.sort.MergeSort;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class FileService {
    private static final String FILE_PATH = "src/main/resources/static/";
    private static Result result = new Result();

    private Map<SortType, CustomSort> sortMap;

    public FileService() {
        sortMap = Map.of(
                SortType.BUBBLE, new BubbleSort(),
                SortType.HEAP, new HeapSort(),
                SortType.MERGE, new MergeSort()
        );
    }

    public void uploadFile(MultipartFile file, SortType sortType) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        List<Student> students = new LinkedList<>();
        while (reader.ready()) {
            String[] str = reader.readLine().split(",");
            students.add(new Student(str[0], Double.parseDouble(str[1])));
        }

        CustomSort customSort = sortMap.get(sortType);
        long before = System.nanoTime();
        customSort.sort(students);
        long after = System.nanoTime();
        saveFile(students);
        result.setStudents(students);
        result.setTime(getTime(before, after));
    }

    public void saveFile(List<Student> students) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_PATH + "result.txt"));
        students.forEach(student -> {
            try {
                writer.append(String.format("%s,%s\n", student.getName(), student.getGrade()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }

    public ResponseEntity<Resource> loadFileAsResource(String fileName) {
        Path path = Paths.get(FILE_PATH + fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    public static Result getResult() {
        return result;
    }

    private String getTime(long before, long after) {
        double diff = (double) (after - before) / 1_000_000_000;
        return String.format("%.6f", diff);
    }
}
