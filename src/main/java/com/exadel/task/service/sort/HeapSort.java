package com.exadel.task.service.sort;

import com.exadel.task.model.Student;

import java.util.Collections;
import java.util.List;

public class HeapSort implements CustomSort {
    @Override
    public void sort(List<Student> students) {
        int n = students.size();

        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(students, n, i);

        for (int i = n - 1; i > 0; i--) {
            Collections.swap(students, 0, i);
            heapify(students, i, 0);
        }
    }

    private void heapify(List<Student> students, int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < n && students.get(l).getGrade() > students.get(largest).getGrade())
            largest = l;

        if (r < n && students.get(r).getGrade() > students.get(largest).getGrade())
            largest = r;

        if (largest != i) {
            Collections.swap(students, i, largest);

            heapify(students, n, largest);
        }
    }
}
