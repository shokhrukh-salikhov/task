package com.exadel.task.service.sort;

import com.exadel.task.model.Student;

import java.util.Collections;
import java.util.List;

public class BubbleSort implements CustomSort {
    @Override
    public void sort(List<Student> students) {
        int n = students.size();
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (students.get(j - 1).getGrade() > students.get(j).getGrade()) {
                    Collections.swap(students, j-1, j);
                }
            }
        }
    }
}
