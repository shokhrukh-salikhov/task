package com.exadel.task.service.sort;

import com.exadel.task.model.Student;

import java.util.List;

public class MergeSort implements CustomSort {
    @Override
    public void sort(List<Student> students) {
        sort(students, 0, students.size() - 1);
    }

    private void sort(List<Student> students, int l, int r) {
        if (l < r) {

            int m = l + (r - l) / 2;

            sort(students, l, m);
            sort(students, m + 1, r);

            merge(students, l, m, r);
        }
    }

    private void merge(List<Student> students, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        Student[] L = new Student[n1];
        Student[] R = new Student[n2];


        for (int i = 0; i < n1; ++i)
            L[i] = students.get(l + i);
        for (int j = 0; j < n2; ++j)
            R[j] = students.get(m + 1 + j);

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i].getGrade() <= R[j].getGrade()) {
                students.set(k, L[i]);
                i++;
            } else {
                students.set(k, R[j]);
                j++;
            }
            k++;
        }

        while (i < n1) {
            students.set(k, L[i]);
            i++;
            k++;
        }

        while (j < n2) {
            students.set(k, R[j]);
            j++;
            k++;
        }
    }
}
