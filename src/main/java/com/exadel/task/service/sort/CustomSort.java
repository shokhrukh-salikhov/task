package com.exadel.task.service.sort;

import com.exadel.task.model.Student;

import java.util.List;

public interface CustomSort {
    void sort(List<Student> students);
}
